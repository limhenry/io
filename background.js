$.backstretch([
	"https://i.imgur.com/TrIbhw9.jpg",
	"https://i.imgur.com/PCvaFqf.jpg",
	"https://i.imgur.com/ThFSWpH.jpg",
	"https://i.imgur.com/3hBR7Uz.jpg",
	"https://i.imgur.com/nH4yQFJ.jpg",
	"https://i.imgur.com/nAJBWtC.jpg",
	"https://i.imgur.com/WNMWpqh.jpg",
	"https://i.imgur.com/emrNS2O.jpg",
	"https://i.imgur.com/MQXAWon.jpg"
  ], {
	fade: 1000,
	duration: 6000
});